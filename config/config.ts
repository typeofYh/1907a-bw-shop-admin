/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { 
      path: '/login', 
      component: '@/pages/login' 
    },
    { 
      path: '/home', 
      component: '@/pages/home/layout',
      routes: [
        {
          path: '/home',  // 首页 不管什么身份都可以访问首页
          component: '@/pages/home/index/index.tsx'
        }
        // 添加路由应该在这
      ]
   },
  ],
  fastRefresh: {},
  proxy: {
    '/api': {
      'target': 'https://bjwz.bwie.com/mall4w',
      'changeOrigin': true,
      'pathRewrite': { '^/api' : '' },
    },
  }
});


