/*
 * @Author: Yh
 * @LastEditors: Yh
 */
export default {
  define: {
    "process.env": {
      BASE_URL: '/api',
      NODE_ENV: 'development'
    },
  }
}