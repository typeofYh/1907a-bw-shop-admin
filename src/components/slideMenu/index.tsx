/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Menu } from 'antd';
import { useEffect, useState, useMemo } from "react"
import { history } from "umi"
import {
  AppstoreOutlined,
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  PieChartOutlined,
  DesktopOutlined,
} from '@ant-design/icons';

const icons = {
  "admin": <AppstoreOutlined />,
  "store": <MenuUnfoldOutlined />,
  "vip": <MenuFoldOutlined />,
  "order": <PieChartOutlined />,
  "system": <DesktopOutlined />
}

const SlideMenu = () => {
  const [items, setItems] = useState([]);
  const menuList = useMemo(() => {
    return JSON.parse(window.sessionStorage.getItem('menuList'));
  },[])
  const formatMenuList = (menulist) => {
    return menulist.map(item => {
      const Icon = item.icon ? icons[item.icon] : null;
      return {
        key: item.menuId,
        label: item.name,
        icon: Icon,
        children: item.list && Array.isArray(item.list) && formatMenuList(item.list)
      }
    })
  }
  const findItem = (key) => {
    let res = null;
    menuList.forEach(item => {
      item.list.forEach(val => {
        if(val.menuId === key * 1){
          res = val;
        }
      })
    })
    return res;
  }
  useEffect(() => {
    try {
      setItems(formatMenuList(menuList))
    }catch(error){
      console.error('侧边栏导航渲染错误')
      console.error(error)
    }
  }, [menuList])

  const onClick = ({key}) => {
    if(key === 'index') {
      history.push('/home');
      return;
    }
    const cur = findItem(key);
    if(cur){
      history.push('/home/'+ cur.url);
    }
  }
  return (
    <Menu
      onClick={onClick}
      mode="inline"
      theme="dark"
      items={[{
        key:'index',
        label: '首页',
        icon: <AppstoreOutlined />,
      },...items]}
    />
  )
}

export default SlideMenu;