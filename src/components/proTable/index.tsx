import { Table, Tooltip, Button, Modal, Transfer, TransferItem, Pagination } from "antd"
import { useEffect, useState, useCallback, useMemo } from "react"
import { SearchOutlined, RedoOutlined, EyeInvisibleOutlined } from "@ant-design/icons"
interface resultData {
  data: any[];
  success: boolean;
  total?: number;
  size?: number; // 每页多少条
}

enum ToolBarKey {
  搜索 = 'search',
  刷新 = 'reload',
  显隐 = 'showHide'
}

const ToolBarDefault = [
  {
    key: ToolBarKey.搜索,
    toolTip: '搜索',
    icon: <SearchOutlined />,
    action: (e, {setSearchFormShow}) => {
      // 控制searchForm展示
      setSearchFormShow((val:boolean) => !val)
    }
  },
  {
    key: ToolBarKey.刷新,
    toolTip: '刷新',
    icon: <RedoOutlined />,
    action: (e, { reload }) => {
      reload();
    }
  },
  {
    key: ToolBarKey.显隐,
    toolTip: '显隐',
    icon: <EyeInvisibleOutlined />,
    action: (e, { openShowHideDialog }) => {
      // 让弹窗展示
      openShowHideDialog(true);
    }
  }
]
interface ToolBarItem {
  key: string;
  toolTip: string;
  icon: any;
  action?: () => void; 
}
interface Props {
  request: () => Promise<resultData>;
  toolbar?: ToolBarItem[] | null;
  columns: any[];
  searchForm: any;
  tableConfig: any;
  paginationConfig: any
}
const formatColumns:TransferItem[] = (columns) => {
  return columns.filter(item => !item.hideShowhideTransfer).map((item: any) => ({
    ...item,
    key: item.key || item.dataIndex,
    title: item.title || '默认title',
    target: 'right'
  }))
}
const ProTable = ({
  request, 
  columns, 
  toolbar = ToolBarDefault, 
  searchForm = null, 
  paginationConfig = {size: 10, current: 1, total: 10},
  tableConfig = {rowKey:'id'}
}: Props) => {
  const [tableData, setTableData] = useState<any[]>([]);
  const [tableLoading, setTableLoading] = useState<boolean>(false);
  const [showHideVisible, setShowHideVisible] = useState<boolean>(false);
  // 默认显隐弹窗的穿梭框的选中数据
  const [showHideSelectedKeys, setShowHideSelectedKeys] = useState([]);
  // 默认显隐弹窗的穿梭框数据
  const [showHideDataSource, setShowHideDataSource] = useState<TransferItem[]>(() => formatColumns(columns));
  // 默认显隐弹窗的穿梭框的左侧选中key
  const showHideTargetKeys = useMemo(() => {
    return showHideDataSource.filter(item => item.target === 'right').map(item => item.key);
  }, [showHideDataSource])
  // 表格的列配置
  const columnsConfig = useMemo(() => {
    // console.log('columnsConfig---',columns, showHideDataSource)
    return columns.filter(({dataIndex, hideShowhideTransfer}) =>  hideShowhideTransfer || showHideDataSource.find(({key}) => key === dataIndex)?.target === 'right');
  }, [columns, showHideDataSource])

  // 搜索表单的显示隐藏
  const [searchFormShow, setSearchFormShow] = useState(true);

  // 分页配置
  const [ pageOption, setPageOption] = useState<any>({
    current: paginationConfig.current, // 当前页
    total: paginationConfig.total, // 总条数
    size: paginationConfig.size // 当前页多少条
  });
  
  const init = async (option = {current:pageOption.current, size: pageOption.size}) => {
    try {
      setTableLoading(true);
      const {data, total} = await request(option);
      setPageOption({ // 分页配置
        ...pageOption,
        size: option.size,
        current: option.current,
        total
      })
      setTableLoading(false);
      setTableData(data);
    }catch(error){
      console.error(error);
    }
  }
  const renderToolBar = useCallback(() => {
    return (
      <div className={'toolbarbox'}>
        {
          Array.isArray(toolbar) && toolbar.map(item => (
            <Tooltip placement="top" title={item.toolTip} key={item.key}>
              <Button onClick={(e) => hanlderToolbarClick(e, item)} shape="circle" icon={item.icon}>{!item.icon ? item.toolTip : ''}</Button>
            </Tooltip>
          ))
        }
      </div>
    )
  },[toolbar])
  useEffect(() => {
    init();
  },[])
  const hanlderToolbarClick = (e, item) => {
    if(item.action){
      item.action(e, {
        reload:init, 
        openShowHideDialog: setShowHideVisible,
        setSearchFormShow
      });
    }
  }
  const hanlderShowHideKeysChange = (nextTargetKeys, direction, moveKeys) => {
    // 操作数据 从左到右 从右到左  moveKeys这次移动key
    setShowHideDataSource((val) => {
      return val.map(item => {
        if(moveKeys.includes(item.key)){
          return {
            ...item,
            target: direction
          }
        }
        return item;
      });
    })
  }
  const handlerSelectChange = (sourceSelectedKeys:string[], targetSelectedKeys:string[]) => {
    setShowHideSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
  };
  const handlerPageChange = (page:number, pageSize:number) => {
    // console.log(page, pageSize)
    init({
      current: page,
      size: pageSize
    })
    // setPageOption({
    //   ...pageOption,
    //   size: pageSize,
    //   current: page
    // })
  }
  return (
    <div>
      {/**表单 */}
      <div className="searchForm" style={{display: searchFormShow ? 'block' : 'none'}}>
        { searchForm }
      </div>
      <div className="toolbar">
        {
          toolbar && renderToolBar()
        }
      </div>
      {/**显隐弹窗 */}
      <Modal
        footer={null}
        title={'显示隐藏列配置弹窗'}
        visible={showHideVisible}
        onCancel={() => setShowHideVisible(false)}
      >
        <Transfer
          dataSource={showHideDataSource}
          selectedKeys={showHideSelectedKeys}
          onSelectChange={handlerSelectChange}
          render={item => item.title}
          targetKeys={showHideTargetKeys}
          onChange={hanlderShowHideKeysChange}
          titles={['隐藏', '显示']}
        >
        </Transfer>
      </Modal>
      <Table
        columns={columnsConfig}
        dataSource={tableData}
        {...tableConfig}
        loading={tableLoading}
        pagination={false}
      />
      {/**分页 */}
      <Pagination 
        total={pageOption.total}
        pageSize={pageOption.size}
        current={pageOption.current}
        size={'small'}
        showSizeChanger
        showQuickJumper
        showTotal={total => `共 ${total} 条`}
        onChange={handlerPageChange}
      />
    </div>
  )
}

export default ProTable;