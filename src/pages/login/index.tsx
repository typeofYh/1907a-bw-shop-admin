/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import styles from './index.less';
import {Form, Input, Button} from "antd"
import { useState, useMemo } from "react"
import { v4 as uuidv4 } from 'uuid';
import { login } from "@/api/user"
import { history } from "umi"
import cookies from "js-cookie"

export default function IndexPage() {
  const [imgUUid, setImgUUid] = useState(uuidv4())
  const imgSrc = useMemo(() => {
    return `/api/captcha.jpg?uuid=${imgUUid}`
  },[imgUUid])
  const onFinish = (value) => {
    // console.log(value);
    // 表单校验规则通过发送接口登陆
    login({
      ...value,
      sessionUUID: imgUUid
    }).then(res => {
      // 存cookie
      cookies.set('Authorization', res.token_type + res.access_token, {
        expires: 1
      })
      history.push('/home');
      window.location.reload(true);
    })
  }
  const onClickCaptcha = () => {
    setImgUUid(uuidv4())
  }
  return (
    <div className={styles.logincontainer}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="账号"
          name="principal"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="密码"
          name="credentials"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="验证码"
          name="imageCode"
          rules={[{ required: true, message: 'Please input your sessionUUID!' }]}
        >
          <Input />
        </Form.Item>
        <img src={imgSrc} onClick={onClickCaptcha}/>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            登陆
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
