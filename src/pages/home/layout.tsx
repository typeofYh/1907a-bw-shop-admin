/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Layout } from 'antd';

const { Header, Sider, Content } = Layout;
import SlideMenu from "@/components/slideMenu"
const HomePage = (props) => {
  console.log(props);
  return <Layout className="container">
    {/* 侧边栏导航 */}
    <Sider>
      <SlideMenu />
    </Sider>
    <Layout>
      <Header>Header</Header>
      <Content>
        {/* 子路由 */}
        { props.children }
      </Content>
    </Layout>
  </Layout>
}


export default HomePage;