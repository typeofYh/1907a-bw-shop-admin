/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import ProTable from "@/components/proTable"
import { Button } from "antd"
import { getProdList } from "@/api/prod"
import { useState } from "react"
const columns = [
  {
    dataIndex: 'title',
    title: '热搜标题'
  },
  {
    dataIndex: 'content',
    title: '热搜内容'
  },
  {
    dataIndex: 'recDate',
    title: '录入时间'
  },
  {
    dataIndex: 'seq',
    title: '顺序'
  }
]
const Pages = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  return (
    <div>
      <Button type="primary" disabled={!selectedRowKeys.length} onClick={() => {
        console.log(selectedRowKeys);
      }}>批量删除</Button>
      <ProTable 
        columns={columns}
        tableConfig={{
          rowKey: 'hotSearchId',
          rowSelection:{
            type: 'checkbox',
            onChange:(selectedRowKeys: any, selectedRows: any) => {
              // console.log(selectedRowKeys, selectedRows)
              setSelectedRowKeys(selectedRowKeys)
            }
          }
        }}
        request={
          async (option) => {
            const {total, records} = await getProdList({
              ...option
            });
            return {
              total,
              data: records,
              success: true
            }
          }
        }
      />
    </div>
  )
}
export default Pages;