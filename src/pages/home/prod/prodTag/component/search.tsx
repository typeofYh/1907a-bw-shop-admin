/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Form, Input, Button, Select } from "antd"

const Status = [
  {
    title: '禁用',
    id: 0
  },
  {
    title: '正常',
    id: 1
  }
]

const SearchForm = () => {
  return (
    <Form
      layout={'inline'}
    >
      <Form.Item label={'标签名称'}>
        <Input placeholder="标签名称" />
      </Form.Item>
      <Form.Item label={'状态'}>
        <Select placeholder="状态" >
          {
            Status.map(item => (<Select.Option key={item.id}>{item.title}</Select.Option>))
          }
        </Select>
      </Form.Item>
      <Form.Item>
        <Button>搜索</Button>
        <Button>清空</Button>
      </Form.Item>
    </Form>
  )
}

export default SearchForm;