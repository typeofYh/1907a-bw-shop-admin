/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import ProTable from "@/components/proTable"
import { Tag, Button } from "antd"
import { getProdTag } from "@/api/prod"
import SearchForm from "./component/search"
export enum Status {
  禁用,
  正常
}
enum DefaultType {
  自定义类型,
  默认类型
}
const Pages = () => {
  return (
    <div>
      <ProTable 
        searchForm={<SearchForm />}
        tableConfig={{
          rowKey: 'id'
        }}
        columns={[
          {
            dataIndex: 'id',
            title: 'id'
          },
          {
            dataIndex: 'title',
            title: '标签名称'
          },
          {
            dataIndex: 'status',
            title: '状态',
            render: (text, {status}) => {
              return <Tag>{Status[status]}</Tag>
            }
          },
          {
            dataIndex: 'isDefault',
            title: '默认类型',
            render: (text, {isDefault}) => {
              return <Tag>{DefaultType[isDefault]}</Tag>
            }
          },
          {
            title: '操作',
            hideShowhideTransfer: true,
            render: () => {
              return <div>
                <Button>修改</Button>
                <Button>删除</Button>
              </div>
            }
          }
        ]}
        request={
          async ({current, size}) => {
            const {total, records} = await getProdTag({
              current,
              size
            });
            return {
              total,
              size,
              data: records,
              success: true
            }
          }
        }
      />
    </div>
  )
}
export default Pages;