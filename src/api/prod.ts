
import { request } from 'umi';
import cookies from "js-cookie"

const { BASE_URL } = process.env; 

interface PageParams {
  current: number;
  size: number
}

export const getProdTag = (params: PageParams = {current:1,size:10}) => request(BASE_URL + '/prod/prodTag/page', {
  params,
  headers: {
    Authorization: cookies.get('Authorization')
  }
})

export const getProdList = (params: PageParams = {current:1,size:10}) => request(BASE_URL + '/admin/hotSearch/page', {
  params,
  headers: {
    Authorization: cookies.get('Authorization')
  }
})