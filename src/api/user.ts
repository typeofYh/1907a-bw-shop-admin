/*
 * @Author: Yh
 * @LastEditors: Yh
 */
//我新添加了内容
//2222
import { request } from 'umi';
import cookies from "js-cookie"

const { BASE_URL } = process.env; 

export const login = (data) => request(BASE_URL + '/login', {
  data,
  method: 'POST'
})

export const menuNav = () => request(BASE_URL + '/sys/menu/nav', {
  headers: {
    Authorization: cookies.get('Authorization')
  }
})

// cookie  服务端可以操作 接口在给你返回数据时可以设置cookie 可以设置过期时间 可以设置domain 可以设置httpOnly cookie的存储体积比较小
// sessionStorage localStorage 只能通过window去操作，只能前端去操作 
// sessionStorage 一次性浏览器会话记录,浏览器窗口关闭自动清除
// localStorage 本地永久存储，用户手动清除才会消失
