/*
 * @Author: Yh
 * 运行时配置
 * @LastEditors: Yh
 */
import "@/style/common.less";
import { menuNav } from "@/api/user"
import { history } from 'umi';

// 动态添加路由
export function patchRoutes({ routes }){
  try{
    const menuList = JSON.parse(window.sessionStorage.getItem('menuList'))
    // 动态遍历menuList 添加路由
    console.log('menuList----', menuList);
    const homeParent = routes.find(item => item.path === '/home');
    menuList.forEach(item => {
      Array.isArray(item.list) && item.list.forEach(val => {
        homeParent.routes.push({
          path: `/home/${val.url}`,
          component: require('./pages/home/'+val.url+'/index.tsx').default
        })
      })
    })
  }catch(error){
    console.error(error);
  }
}

export async function render(oldRender) {
  console.log('render');
  if(history.location.pathname !== '/login'){
    try {
      const res = await menuNav()
      window.sessionStorage.setItem('menuList', JSON.stringify(res.menuList));
      window.sessionStorage.setItem('authorities', JSON.stringify(res.authorities));
      // 添加路由
      oldRender()
    }catch(error){
      if(error?.response?.status === 401 || error?.response?.status === 403){ // 用户未登录或者没有权限
        history.replace('/login');
        oldRender();
      }
    }
  } else {
    oldRender();
  }
}